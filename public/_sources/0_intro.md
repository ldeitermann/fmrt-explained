# Wilkommen!

Unsere Wahrnehmung ist etwas was uns täglich umgibt. Wobei - umgeben ist untertrieben, durch sie nehmen wir die Welt und alles in ihr überhaupt wahr. Wie unterschiedlich wir alle die Welt wahrnehmen kann man nicht sagen. Aber es gibt einige Phänomene, die mittlerweile bekannt sind. Zum Beispiel die "Aphantasie", das Fehlen des sogenanntem "inneren Auge". Andere Dinge wie zum Beispiel die Schmerzwahrnehmung, können durch bestimmte Methoden messbar gemacht werden. Bis wir wissen wie es wirklich in uns aussieht wird es noch dauern, oder vielleicht wird es auch nie dazu kommen. Aber durch Methoden wie zum Beispiel dem fMRT kommen wir dem immer näher.
Im Folgenden wird es um Sprechen durch unsere Gedanken, das fehlende innere Auge und die Zukunft vom Gedankenlesen gehen. Du kannst sogar einmal selber zum Forscher werden und mit einer Patientin durch ihre Gedanken sprechen! Klick dazu auf den Artikel den du lesen möchtest.



```{tableofcontents}
```





```{seealso}
Jupyter Book uses [Jupytext](https://jupytext.readthedocs.io/en/latest/) to convert text-based files to notebooks, and can support [many other text-based notebook files](https://jupyterbook.org/file-types/jupytext.html).
```


```{note}
Here is a note
```