# Die Zukunft: Ausblick und Möglichkeiten

### Fragen die es zu klären gilt
Wo sich Dinge weiterentwicklen, kommen auch neue Fragen auf. Die Frage, ob Kommunikation durch Gedanken und Sprechen gleichwertig zu betrachten sind, kommt zum Beispiel bei der Beschäftigung mit [Kommunikation durch Gedanken](1_Gedanken-sprechen.ipynb) auf.

Was würdest du zum Beispiel machen, wenn bei der Kommunikation mit einem Menschen via fMRT die Antwort herauskäme, dass die Person nicht mehr leben möchte? Sind die Gedanken eines Menschen, oder eher gesagt die dadurch erzeugten Hirnbilder weiter weg von der Wahrheit als Sprechen? Oder näher dran?

Angenommen man könnte Risikofaktoren für bestimmte Krankheiten im fMRT feststellen - wäre das förderlich? Was könnten für Probleme auftreten?

### Möglichkeiten der Zukunft

All diese Fragen sind wichtig zu klären, besonders wenn man zukünftige Anwendungen betrachtet. Eine faszinierende Idee ist es, die eigenen Träume sichtbar zu machen. Als erster Schritt in diese Richtung sollten in einem Experiment teilnehmende Personen im fMRT Scanner schlafen und wurden zwischendurch geweckt und gefragt was sie geträumt haben. Diese Informationen wurden mit der gemessenen Hirnaktivität gematched und das daraus entstehende Programm konnte bei weiteren Durchgängen zu 60%iger Zuverlässigkeit sagen, von welcher Objektkategorie (wie Autos, Menschen...) geträumt wurde.<sup>[1](#id1)

Ein anderes Beispiel wäre die Anwendung von fMRT vor Gericht. 
Möglich wäre die Untersuchung der Verlässlichkeit von Erinnerungen oder Verzerrungen in der Wahrnehmung von Richtern.  
Bereits Ergebnisse in die Richtung gibt es in dem Anwendungsszenario Verdächtige mithilfe des fMRts für schuldig zu erklären. Bei einem Experiment konnte man mit 80% sagen, ob die ProbandInnen bereits in einem gezeigten Haus waren, oder nicht. Fertig fürs Gericht, ist das natürlich trotzdem nicht. Problem ist zum Beispiel, dass vielleicht auch ein ähnlich aussehendes Haus die Hirnaktivität hervorrufen kann, oder man einfach zu Besuch war. Deswegen haben Anwendungsszenarios, zumindest bis jetzt, meist noch offene Fragen und Probleme, die geklärt werden müssen.<sup>[2](#id2)
    
Aber wäre es nicht trotzdem toll eines Tages einen Film unserer eigenen Träume zu sehen? 




## Referenzen
##### 1
Horikawa, T., Tamaki, M., Miyawaki, Y. & Kamitani, Y. (2013). Neural decoding of visual imagery during sleep. Science (New York, N.Y.), 340(6132), 639–642. https://doi.org/10.1126/science.1234330
    
##### 2
Smith, K. Brain decoding: Reading minds. Nature 502, 428–430 (2013). https://doi.org/10.1038/502428a




