# Können wir durch unsere Gedanken sprechen?
Unsere Sprache ist von der Funktionalität unseres neuromuskulären Systems abhängig. Das heißt, wenn zum Beispiel eine motorische Einschränkung vorliegt, können manche Menschen trotz vollem Bewusstsein nicht kommunizieren. Sichtbar wird dies am Beispiel des "Locked-In-Syndroms". Das Locked In Syndrom xxxx

_Sorger et al. (2012)_

Doch welche Chancen bieten neurowissenschaftliche Methoden wie das fMRT in so einem Fall?

Zunächst einmal gelang es einer Forschungsruppe rund um Adrian Owen im Jahre 2006 ein Bewusstsein bei PatientInnen im sogenannten "vegetativen Zustand" festzustellen. Diser Zustand zwichnet sich darin aus, dass PatientInnen die aus dem Koma erwachen wach zu sein scheinen, aber keine Anzeichen von Bewusstsein zeigen. 

- Frage nach Tennis spielen oder Zuhause rumlaufen
- Man würde denken: nur auditive Areale
- aber: Kortikale Areale aktiviert, wie bei gesunder Kontrollgruppe

> Bild von Hirn bei Tennis VS Hirn bei Wohnung, mit erklärender Unterschrift

- aus dieser revolutionären Erkenntnis enstand die Idee der selben Forschungsgruppe, über genau diese Gehirnareale zu kommunizieren. Sie stellten die selben Aufgaben, wobei in diesem Fall "Tennis spielen" ein Ja und "durch die Wohnung laufen" ein "Nein" bedeutete. Mithilfe dieser Antwortmöglichkeiten konnten der Patientin Fragen zu ihrer Biographie gestellt werden. Die Patientin antwortete über die mentalen Aufgaben und die Forschungsgruppe konnte die Antworten durchd ie entstehenden fMRT Bilder verstehen. 
Doch noch einmal zurück zum Anfang, unsere Sprache ist Abhängig von einem funktionierenden Neuromuskulären System. Aber ist unsere Kommunikation es auch? 

Ein paar Jahre später, 2012, entwickelte die Gruppe um Bettina Sorger eine fMRT basierte Buchstabiertechnik, basierend auf dem gleichen Prinzip, dass Owen et al. einige Jahre vorher entdeckten. 
- Experiment beschreiben
Und bis heute gibt es weitere Ideen diesen Ansatz zu verfolgen und zu verbessern. Es werden portable Geräte genutzt (sogenannte fNIRS) oder * neue Forschungsergebnisse einsetzten *

- Frage stellen nach Gleichwertigkeit

Wenn du bis hierhin gelesen hast, dann möchtest du bestimmt einmal selber forschen und über das fMRT kommunizieren! 

> Aufgabe unter dem "An welche Person denke ich" Prinzip
