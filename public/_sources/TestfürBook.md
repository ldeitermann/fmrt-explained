# Titeländerung

This is some sample text.

(section-label)=
## Titel 2

Here is a [reference to the intro](intro.md). Here is a reference to [](section-label).


## Brain decoding: Reading minds, Kerri Smith (2013)

_Smith, K. Brain decoding: Reading minds. Nature 502, 428–430 (2013)._ _https://doi.org/10.1038/502428a_ _https://youtu.be/z8iEogscUl8_

---
> #### Aufbereitung Artikel
> * Start mit lebendigem Beispiel (Forscher sitzt da, an der eine Seite dies, an der anderen das...)
> #### Fragen:
> * gibt es Neuigkeiten und Weiterentwicklungen? #19
---

#### Inhaltlich

* Jack Gallant <span dir="">had trained a program to decode fmri data, by showing it patterns of brain activity elicited by a range of images and film clips</span> ("<span dir="">the computer program uses only the details of that scan to guess what the participant was watching at the time</span>")
* its is possible to <span dir="">decode brain scans and decipher what people are seeing, hearing and feeling, as well as what they remember or even dream about</span>
* ![image](uploads/b05cd98526db1dd8852657d0cfa8efc6/image.png)
* <span dir="">Decoding techniques interrogate more of the information in the brain scan. Rather than asking which brain regions respond most strongly to faces, they use both strong and weak responses to identify more subtle patterns of activity</span>
* <span dir="">These recordings are fed into a 'pattern classifier', a computer algorithm that learns the patterns associated with each picture or concept. Once the program has seen enough samples, it can start to deduce what the person is looking at or thinking about</span>
* <span dir="">Russell Poldrack (fMRI specialist, University of Texas, Austin): "decoding allows researchers to test existing theories from psychology that predict how people's brains perform tasks</span>"
  * for example: i<span dir="">t was known from studies using electrodes implanted into monkey and cat brains that many visual areas react strongly to the orientation of edges</span>, in human fmri it couldnt be seen (areas too small) -> but by applying decoding techniques, it became visible
    * <span dir="">The researchers showed volunteers lines in various orientations — and the different voxel mosaics told the team which orientation the person was looking at</span>
  * Counterstrike: <span dir="">researchers tried to see if they could decode an intention to go left or right, chase an enemy or fire a gun. They could just about decode an intention to move around; but everything else in the fMRI data was swamped by the signal from participants' emotions when they were being fired at or killed in the game. These signals — especially death, says Gallant — overrode any fine-grained information about intention.</span>
  * Dreams: <span dir="">They let participants fall asleep in the scanner and then woke them periodically, asking them to recall what they had seen. The team tried first to reconstruct the actual visual information in dreams, but eventually resorted to word categories. Their program was able to predict with 60% accuracy what categories of objects, such as cars, text, men or women, featured in people's dreams.</span>
* Reverse Engineering: <span dir="">Decoding relies on the fact that correlations can be established between brain activity and the outside world</span>
  * sufficient if you want to use a signal from the brain to command a robot hand
  * but not if you want to "crack the codes"
    * not easy: <span dir="">Each brain area takes information from a network of others and combines it, possibly changing the way it is represented. Neuroscientists must work out </span>_post hoc_<span dir=""> what kind of transformations take place at which points</span>
* One fits all?: <span dir="">Devising a decoding model that can generalize across brains, and even for the same brain across time, is a complex problem. Decoders are generally built on individual brains, unless they're computing something relatively simple such as a binary choice — whether someone was looking at picture A or B</span>
* Anwendung:
  * <span dir="">decode hidden consumer preferences of test subjects for market research</span> (not possible in 2013, but in theory)
  * <span dir="">No Lie MRI in San Diego, California, for example, is using techniques related to decoding to claim that it can use a brain scan to distinguish a lie from a truth</span>
  * <span dir="">detecting lies, checking the reliability of memories, or even revealing the biases of jurors and judges</span>
  * -> stehen Geadnken über Worten? Sollte man Gedanken nutzen dürfen?

## Detecting Awareness in the Vegetative State

DOI: 10.1126/science.1130197

#### Abstract

We used functional magnetic resonance imaging to demonstrate preserved conscious awareness in a patient fulfilling the criteria for a diagnosis of vegetative state. When asked to imagine playing tennis or moving around her home, the patient activated predicted cortical areas in a manner indistinguishable from that of healthy volunteers.

> vegetative state = unique disorder in which patients who emerge from coma appear to be awake but show no signs of awareness -> ethically troblesome, not well understood

* Diagnosis depends on no **no reproducible evidence of purposeful behavior in response to external stimulation**, but recent functional neuroimaging studies have suggested that **“islands” of preserved brain function** may exist in a small percentage of patients who have been diagnosed as vegetative
* eine Patientin bei der "vegetative state"

In der Studie von xxx wurde eine Patientin, die nach einem Unfall mit schweren Hirnverletzungen der "vegetative state" diagnostiziert wurde, in einem fMRT Scanner untersucht. Hierbei wurden ihr Sätze vorgespielt, welche eine AKtivität in sprachspezifischen Regionen auslösten, ähnlich wie bei der gesunden Kontrollgruppe. `` bilaterally in the middle and superior temporal gyri `` Bei zweideutigen Sätzen, war die Aktivität, wie auch bei der Kontrollgruppe, in der `` left inferior frontal region `` höher. Das zeigt, dass vermutlich semantische Prozesse abgelaufen sind, welche entscheidend für das Sprachverstehen sind.
Eine neuronale Antwort auf Gesprochenes ist allerdings kein Beweis für Bewusstsein. In mehreren Studien wurde gezeigt, dass mannche Prozesse auch ohne aktives Bewusstsein ablaufen.
Deswegen hat man die Patientin ein weiteres mal im fMRT Scanner untersucht. Dieses Mal hatte sie zwei Aufgaben, einmal sich vorzustellen Tennis zu spielen und in der anderen Aufgabe, sich vorzustellen durch ihre Wohnung zu gehen. Das fMRT zeigte, dass bei der Vorstellung vom Tennisspielen motorische Areale aktiv waren `` supplementary motor area `` und beim mentalen Gang durch die Wohnung Gehirnareale, die auch bei gesunden Menschen bei einer solchen Aufgabe aktiv sind `` parahippocampal gyrus, the posterior parietal cortex, and the lateral premotor cortex ``.
![image](uploads/2bee36f2ad5a1297fe086f81c3d4dccf/image.png)

Die Ergebnisse zeigen, dass obwohl die Patientin sich in einem diagnostizierten, vegetativen Zustand befindet, trotzdem auf Anweisungen reagieren konnte. Dies geschah nicht durch Bewegung oder Sprache, sondern durch ihre Hirnaktivität. Dass sie bei den Aufgaben kooperiert hat zeigt eine klare Intention, was darauf schließen lässt, dass sie sich ihrer Umgebung und sich bewusst ist.

## Willful Modulation of Brain Activity in Disorders of Consciousness - Monti et al. (2010)
PatientInnen mit schweren Hirnverletzungen, denen entweder der "minimal conscious state" oder "vegetative state" diagnostiziert wurde, wurden mithilfe eines fMRT Scans untersucht. Es ging darum herauszufinden wie groß die Häufigkeit von nicht entdeckter "Awareness" ist. Hierbei wurden zunächst einmal Aufgaben gestellt, um zu sehen ob sie auf Anweisungen reagieren (siehe oben). Danach wurden mithilfe von "Tennis spielen" als ja und "durch Wohnung gehen" als nein, Fragen zur Biographie beantwortet.
Von den 54 Patienten waren 5 in der Lage, ihre Hirnaktivität zu modulieren, indem sie freiwillige, zuverlässige und wiederholbare, vom Sauerstoffgehalt des Blutes abhängige Reaktionen in vordefinierten neuroanatomischen Regionen hervorriefen, wenn sie aufgefordert wurden, Bildaufgaben auszuführen.
Von den 5 Patient*innen, haben 2 bis zum Ende der Studie keine "behaviorally response" gezeigt. Daraus lässt sich ableiten, dass eine Mindehreit von PatientInnen, die die Verhaltenskriterien für einen vegetativen Zustand erfüllen kognitive Restfunktionen oder sogar ein Bewusstsein haben.

## A Real-Time fMRI-Based Spelling Device Immediately Enabling Robust Motor-Independent Communication DOI: 10.1016/j.cub.2012.05.022 (2012)

- Menschliche Kommunikation ist komplett abhängig von der Funktionalität des **neuromuskulären Systems**
- sichtbar wird das an zum Beispiel dem "Locked-in-Syndrom", bei dem PatientInnen nicht kommunizieren können, weil sie motorisch eingeschränkt sind -> trotz vollem Bewusstseins
- Methoden der Kommunikation wurden entwickelt: Brain Computer Interfaces, basierend auf neuroelektischen Signalen oder EEG
- im Folgenden geht es um die *Erfindung einer "spelling device based on fMRI"
- Idee: durch räumlich-zeitliche Merkmale der hämodynamischen Reaktionen, welche durch Timing und Ausführung verschiedener "mental imagery tasks" erreicht werden, kann buchstabiert werden
- so kann - Buchstabe für Buchstabe - kommuniziert werden
- Vorteile: wenig benötigter Aufwand und Training, weschelseitige Kommunikation innerhalb einer Scanning Session, nicht-invasiv -> hohes Potenzial für die klinische Anwendung (diagnostisch und kommunikativ)
- Input muss nicht unbedingt visuell "geschehen", sondern kann auch durch auditorische Signale gegeben sein
- ![image](uploads/fe86198a139e7afe8b3a3ca61fc7a1eb/image.png)

- PatientInnen musstesn sich das nicht alles merken, sondern hatten eine "Tafel" vor sich, auf der die "Anleitung" stand
- als aktivierte Hinregionen wählte man Regionen die stabil und "differenziert" aktivierten 

- es gibt Potenzial die Encoding-Time zu verkürzen, zum Beispiel durch multivoxel pattern analysis
- MRI muss irgendwann mit mobile equipment (fNIRS) ersetzt werden 
! nur mit gesunden freiwilligen probiert! -> aber Situation "nachgestellt"

## Single-session communication with a locked-in patient by functional near-infrared spectroscopy (2017)  https://doi.org/10.1117/1.NPh.4.4.040501
- Vorteile **time-resolved functional near-infrared spectroscopy (TR-fNIRS)**: geringe kosten, tragbarkeit, erhöhte Sensitivität gegebnüber Gehirnaktivität
- Ergebnis: " four-channel TR-fNIRS system" konnte befehlsgesteuerte Hirnaktivität bei einem locked-in Patienten feststellen

## Brain-Based Binary Communication Using Spatiotemporal Features of fNIRS Responses (2020)

## Assessing Time-Resolved fNIRS for Brain-Computer Interface Applications of Mental Communication (2020)
