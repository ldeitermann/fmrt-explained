# Wilkommen!

Unsere Wahrnehmung ist ein ständiger Begleiter in unserem täglichen Leben. Wobei - ständiger Begleiter ist untertrieben, sie ist unser Blick auf die Welt und alles in ihr. Wie unterschiedlich wir unsere Umgebung wahrnehmen, kann man nicht sagen. Aber es gibt einige Phänomene, die mittlerweile bekannt sind. Zum Beispiel die "Aphantasie", das Fehlen des sogenannten "inneren Auges". Andere Dinge wie zum Beispiel die Schmerzwahrnehmung, können durch bestimmte Methoden messbar gemacht werden.  Obwohl wir noch nicht vollständig verstehen, wie unser Wahrnehmungsprozess funktioniert, bringen uns Technologien wie die funktionelle Magnetresonanztomographie (fMRT) näher an ein tieferes Verständnis.
Im Folgenden wird es um Sprechen durch unsere Gedanken, das fehlende innere Auge und die Zukunft des Gedankenlesen gehen. Du kannst sogar einmal selber zum Forscher/zur Forscherin werden und mit einer Patientin durch ihre Gedanken sprechen! Klick dazu auf den Artikel den du lesen möchtest.


```{tableofcontents}
```